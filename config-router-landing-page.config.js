(function () {
    function configLanding($urlMatcherFactoryProvider, $stateProvider, PATH_CONFIG) {

        if (data.defaultLandingPage == undefined || data.defaultLandingPage == true) {

            $urlMatcherFactoryProvider.strictMode(false);

            //$stickyStateProvider.enableDebug(true);
            // We create an array to store the ids of each identifier
            var arrayRouting = [];

            setTimeout(function () {

                for (var key in PATH_CONFIG.routing) {

                    var key = PATH_CONFIG.routing[key];

                    arrayRouting[key.identifier] = key.id;

                    // Creation of different variables to provide to the router if the route is not a redirection
                    if (typeof key.redirect_object == 'undefined') {

                        if (typeof key.route != 'undefined' && key.identifier != 'error_page') {

                            if (key.identifier == 'landing_pages') {
                                var landingKey = key;

                                $stateProvider.state('top.' + landingKey.identifier, {
                                    name: 'top.' + landingKey.identifier,
                                    url: '/' + PATH_CONFIG.current_language + landingKey.route,
                                    onEnter: onLandingEnter,
                                    views: {
                                        'main': {
                                            template: '<loader-template></loader-template>',

                                            resolve: {
                                                promiseObj: function ($stateParams, $q, $http, $rootScope) {
                                                    var defer = $q.defer();
                                                    var url = host + $stateParams.url + $rootScope.previewHash;

                                                    url = ($stateParams.url.indexOf('{landing_page_id}') != -1) ? url.replace('{landing_page_id}', $stateParams.id) : url;

                                                    $http.get(url).then(function (response) {
                                                        defer.resolve(response.data[0]);

                                                        if (typeof response.data[0] == 'undefined') {
                                                            //$rootScope.showMenu = true;
                                                        } else if (response.data[0].content.landing_page_type != 3) {
                                                            $rootScope.showMenu = false;
                                                        }
                                                    });

                                                    return defer.promise;
                                                }
                                            },
                                            controller: function pageController($scope, promiseObj, $rootScope, $state, preRenderService) {
                                                $scope.modulesController = promiseObj;

                                                if (typeof promiseObj != 'undefined') {

                                                    if ($scope.modulesController.content.landing_page_type != 3) {

                                                        //$rootScope.showMenu = true;
                                                    } else {
                                                        $rootScope.showMenu = false;
                                                        if ($rootScope.firstLoad) {
                                                        }
                                                    }

                                                    if ($state.params.identifier == 'top.landing_pages' && typeof $state.params.subpage_id == 'undefined') {


                                                        // location to be added in prerender status code
                                                        var location = $state.href('top.landing_pages.landing_subpages', {
                                                            id: $state.params.id,
                                                            slug: $scope.modulesController.content.menu_items[0].landing_page_slug,
                                                            subpage_id: $scope.modulesController.content.menu_items[0].landing_subpage_id,
                                                            subpage_slug: $scope.modulesController.content.menu_items[0].landing_subpage_slug
                                                        }, {absolute: true});

                                                        // Redirection
                                                        var redirectLocationIdentifier = 'top.landing_pages.landing_subpages';
                                                        var redirectLocationParams = {
                                                            id: $state.params.id,
                                                            slug: $state.params.slug,
                                                            subpage_id: $scope.modulesController.content.menu_items[0].landing_subpage_id,
                                                            subpage_slug: $scope.modulesController.content.menu_items[0].landing_subpage_slug
                                                        };

                                                        preRenderService.state301(location, redirectLocationIdentifier, redirectLocationParams);

                                                    } else {
                                                        if ($state.params.slug != $scope.modulesController.content.menu_items[0].landing_page_slug) {

                                                            // location to be added in prerender status code
                                                            var location = $state.href('top.landing_pages.landing_subpages', {
                                                                id: $state.params.id,
                                                                slug: $scope.modulesController.content.menu_items[0].landing_page_slug,
                                                                subpage_id: $scope.modulesController.content.menu_items[0].landing_subpage_id,
                                                                subpage_slug: $scope.modulesController.content.menu_items[0].landing_subpage_slug
                                                            }, {absolute: true});

                                                            // Redirection
                                                            var redirectLocationIdentifier = 'top.landing_pages.landing_subpages';
                                                            var redirectLocationParams = {
                                                                id: $state.params.id,
                                                                slug: $scope.modulesController.content.menu_items[0].landing_page_slug,
                                                                subpage_id: $state.params.subpage_id,
                                                                subpage_slug: $state.params.subpage_slug
                                                            };

                                                            preRenderService.state301(location, redirectLocationIdentifier, redirectLocationParams);

                                                        }
                                                    }

                                                } else {

                                                }


                                                // Enables the scrollTop on loading a new page
                                                $('body, html').animate({
                                                    scrollTop: 0
                                                }, 0);

                                            }
                                        }
                                    },
                                    params: {
                                        identifier: 'top.' + landingKey.identifier,
                                        url: landingKey.url,
                                        route: '/' + PATH_CONFIG.current_language + landingKey.route.replace('{id}/', '{id}')
                                    }
                                });

                                if (typeof landingKey.routes != 'undefined') {

                                    for (var j = 0; j < landingKey.routes.length; j++) {

                                        $stateProvider
                                            .state('top.' + landingKey.identifier + '.' + landingKey.routes[j].identifier, {
                                                name: 'top.' + landingKey.identifier + '.' + landingKey.routes[j].identifier,
                                                url: '{subpage_slug}/{subpage_id}/',
                                                views: {
                                                    'subpage': {
                                                        templateUrl: PATH_CONFIG.EXTEND + 'templates/landing_subpages.template.html',
                                                        resolve: {
                                                            subpage: function ($stateParams, $q, $http, metaTags, $timeout, $rootScope, $state, preRenderService, $injector) {
                                                                var defer = $q.defer();

                                                                var url = host + 'api/library/landingpages/{landing_page_id}/{landing_subpage_id}?lang=' + PATH_CONFIG.current_language + $rootScope.previewHash;

                                                                url = url.replace('{landing_page_id}', $stateParams.id).replace('{landing_subpage_id}', $stateParams.subpage_id);

                                                                $http.get(url).then(function (response) {
                                                                    defer.resolve(response.data[0]);

                                                                    if (typeof response.data[0] != 'undefined') {
                                                                        // Indxation Check
                                                                        $rootScope.robotsMetaContent = (typeof response.data[0].content.disable_indexation != 'undefined' && response.data[0].content.disable_indexation == 1) ? 'noindex, nofollow, noarchive' : 'index, follow';

                                                                        // Redirecting if the subpage slug is wrong
                                                                        if (response.data[0].content.slug != $stateParams.subpage_slug) {

                                                                            // location to be added in prerender status code
                                                                            var location = $state.href('top.landing_pages.landing_subpages', {
                                                                                id: $stateParams.id,
                                                                                slug: $stateParams.slug,
                                                                                subpage_id: $stateParams.subpage_id,
                                                                                subpage_slug: response.data[0].content.slug
                                                                            }, {absolute: true});

                                                                            // Redirection
                                                                            var redirectLocationIdentifier = 'top.landing_pages.landing_subpages';
                                                                            var redirectLocationParams = {
                                                                                id: $stateParams.id,
                                                                                slug: $stateParams.slug,
                                                                                subpage_id: $stateParams.subpage_id,
                                                                                subpage_slug: response.data[0].content.slug
                                                                            };

                                                                            preRenderService.state301(location, redirectLocationIdentifier, redirectLocationParams);

                                                                        }

                                                                        if (typeof response.data != 'undefined') {
                                                                            $timeout(function () {
                                                                                metaTags.updateMetaTags(response.data[0]);
                                                                            }, 10);

                                                                            $rootScope.landingPageBlocks = response.data[0].content.blocks;

                                                                        }
                                                                    } else {

                                                                        // Redirect to Error page.
                                                                        // This is the case where the landing sub page could be offline.

                                                                        // API call to check the page if there is a redirection setted up.
                                                                        var redirectService = $injector.get('redirectService');
                                                                        redirectService.redirectCall();
                                                                    }
                                                                });


                                                                return defer.promise;
                                                            }

                                                        },
                                                        controller: function pageController($scope, $rootScope, subpage, $stateParams, APP_CONFIG, $timeout) {
                                                            $scope.modulesController = subpage;

                                                            if (typeof subpage != 'undefined') {

                                                                // SubBrands theme & favicon
                                                                if ($scope.$parent.$parent.content.content.landing_page_type == 3) {
                                                                    $rootScope.theme = $stateParams.slug;

                                                                    $rootScope.showMenu = false;
                                                                } else {
                                                                    // Landing Pages theme & favicon
                                                                    var themeDefined = false;

                                                                    // defines a special theme form parameters.js
                                                                    if (typeof data.specialTheme != 'undefined') {
                                                                        for (var i = 0; i < data.specialTheme.length; i++) {
                                                                            $rootScope.theme = (typeof data.specialTheme[i][$stateParams.identifier] != 'undefined') ? data.specialTheme[i][$stateParams.identifier] : $rootScope.theme;
                                                                            themeDefined = true;
                                                                        }
                                                                    }

                                                                    if (!themeDefined) {
                                                                        $rootScope.theme = data.theme;
                                                                    }

                                                                    // Defines the Theme from the settings call
                                                                    for (var i = 0; i < APP_CONFIG.length; i++) {
                                                                        if (APP_CONFIG[i].content.content_type == 'theme') {
                                                                            $rootScope.theme = 'theme-' + APP_CONFIG[i].content.colors.color_schema;
                                                                        }
                                                                    }
                                                                }
                                                                ;

                                                                $('.template-2__view').removeClass('shown');


                                                                // loads the theme by page, if it is found in parameters,
                                                                // in array "specialTheme"
                                                                $rootScope.$emit('loadTheme', $rootScope.theme);

                                                                // We define it false so the menu is shown properly.
                                                                $rootScope.firstLoad = false;

                                                                // code custom by paradiso project
                                                                $timeout(function () {

                                                                    var leftColumn = $('.template-landing-subpages__column-left');
                                                                    var leftColumnLinks = $('[ui-view="subpage"]');
                                                                    var leftColumnOffset;
                                                                    var leftColumnPaddingTop;
                                                                    var offset;

                                                                    if (data.enableScrollLandingPage) {
                                                                        $(window).on('scroll', function () {
                                                                            var sc = $(this).scrollTop();
                                                                            leftColumnOffset = leftColumnLinks.offset().top;
                                                                            leftColumnPaddingTop = leftColumn.css('padding-top');
                                                                            offset = 30 - parseInt(leftColumnPaddingTop);
                                                                            if ($(window).width() > 768) {

                                                                                if (sc >= leftColumnOffset - offset) {
                                                                                    leftColumn.addClass('fixed');
                                                                                    leftColumn.css('top', offset);
                                                                                } else {
                                                                                    leftColumn.removeClass('fixed');
                                                                                    leftColumn.css('top', 0);
                                                                                }
                                                                            } else {
                                                                                leftColumn.removeClass('fixed');
                                                                                leftColumn.css('top', 0);
                                                                            }
                                                                        });
                                                                    }
                                                                    ;

                                                                }, 0);

                                                            } else {
                                                                //$rootScope.showMenu = true;
                                                            }

                                                        }
                                                    }
                                                },

                                                params: {
                                                    identifier: 'top.' + landingKey.identifier + '.' + landingKey.routes[j].identifier,
                                                    url: landingKey.routes[j].url,
                                                    route: '{subpage_slug}/{subpage_id}/'
                                                },

                                                onEnter: onLandingEnter
                                            });

                                    }
                                }

                                function onLandingEnter($previousState, $rootScope, $timeout) {

                                    if ($rootScope.extendIndexLoaded) {
                                        triggerLandingItem($previousState, $rootScope, $timeout);
                                    }

                                    $rootScope.$on('extendIndex', function () {
                                        triggerLandingItem($previousState, $rootScope, $timeout);
                                    });

                                }

                                function triggerLandingItem($previousState, $rootScope, $timeout) {
                                    if ($previousState.get() == null) {
                                        $rootScope.firstLoad = true;
                                        $rootScope.modalClass = '';
                                    } else {
                                        $rootScope.firstLoad = false;
                                        $rootScope.modalClass = '';
                                    }

                                }

                                onLandingEnter.$inject = ['$previousState', '$rootScope', '$timeout'];

                            }
                        }
                    }
                }

            }, 0);
        }
    }

    configLanding.$inject = ['$urlMatcherFactoryProvider', '$stateProvider', 'PATH_CONFIG'];

    angular
        .module('bravoureAngularApp')
        .config(configLanding);

})();
