## Angular Config Router Landing Page / Bravoure component

This Component generates the router for landing pages and creates the routes for them.


### **Versions:**

1.0.7   - Fix for the subpage redirection

1.0.6   - new tag added for making the redirect call

1.0.5   - Updated redirection When subpage is offline

1.0.4   - Added condition to redirect to error page when subpage is offline

1.0.3   - Added variable to control the blocks of the landing subpage

1.0.2   - Indexation, added check for indexation in subpages

1.0.1   - Added previewHash Variable to be added to urls on ajax calls

1.0     - Initial Stable version

---------------------
